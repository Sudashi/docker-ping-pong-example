const express = require('express');
const axios = require('axios');

const app = express();
const port = 3001;


app.get('/ping', (req, res) => {
  // Wait for one second before making the GET request to "localhost:3002/pong"
  console.log("pong");

  setTimeout(() => {
    axios.get('http://pong:3001/pong')
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        console.log(error);
        res.sendStatus(500);
      });
  }, 1000);
});

app.get('/', (req, res) => {
  res.send({ msg: 'ping is running!' });
})

setTimeout(() => {
  axios.get('http://pong:3001/pong')
    .then(response => {
      res.send(response.data);
    })
    .catch(error => {
      console.log(error);
      res.sendStatus(500);
    });
}, 3000);

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
