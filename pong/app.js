const express = require('express');
const axios = require('axios');

const app = express();
const port = 3001;

app.get('/pong', (req, res) => {
  // Wait for one second before making the GET request to "localhost:3001/ping"
  console.log("ping");

  setTimeout(() => {
    axios.get('http://ping:3001/ping')
      .then(response => {
        res.send(response.data);
      })
      .catch(error => {
        console.log(error);
        res.sendStatus(500);
      });
  }, 1000);
});

app.get('/', (req, res) => {
  res.send({ msg: 'pong is running!' });
})

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
